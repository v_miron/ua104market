package pageObjects;

import org.openqa.selenium.WebElement;


public class CallbackPage extends BasePage {

    String callBackBtnXpath = "//a[@href='#callback__Popup']";
    String nameFieldXpath = "//input[@name='callback-firstname']";
    String phoneFieldXpath = "//input[@name='callback-phone']";
    String submitBtnXpath = "//input[@type='submit']";
    String errorNameXpath = "//div[@id='callback-popup-form-firstname-error']";
    String errorPhoneXpath =
            "//div[@id='callback-popup-form-callback-phone-error']";
    String successMessage = "//div[contains(@class, 'success')]";

    /**
     * @return true if callback message is sent
     */
    public boolean IsSuccessMessage () {
        WebElement success = waitForElement(successMessage);
        return success.isDisplayed();
    }

    /**
     * @return true if there is name field error message
     */
    public boolean IsErrorName() {
        WebElement nameError = waitForElement(errorNameXpath);
        return nameError.isDisplayed();
    }

    /**
     * @return true if there is phone field error message
     */
    public boolean IsErrorPhone() {
        WebElement phoneError = waitForElement(errorPhoneXpath);
        return phoneError.isDisplayed();
    }

    /**
     * Opens CallBack popup
     */
    public void OpenCallback() {
        WebElement callbackBtn = waitForElement(callBackBtnXpath);
        callbackBtn.click();
    }

    /**
     * Sends data to name field in CallBack popup
     * @param name data we want to send to the name field
     */
    public void SendName(String name) {
        WebElement nameField = waitForElement(nameFieldXpath);
        WebElement phoneField = waitForElement(phoneFieldXpath);
        nameField.sendKeys(name);
        phoneField.click();
    }

    /**
     * Sends data to phone field in CallBack popup
     * @param phone data we want to send to the phone field
     */
    public void SendPhone(String phone) {
        WebElement phoneField = waitForElement(phoneFieldXpath);
        WebElement nameField = waitForElement(nameFieldXpath);
        phoneField.sendKeys(phone);
        nameField.click();
    }

    //

    /**
     * Clicks submit button on callback popup
     */
    public void SubmitCallback() {
        WebElement submitBtn = waitForElement(submitBtnXpath);
        submitBtn.click();
    }

}
