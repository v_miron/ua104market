package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ReviewPage extends pageObjects.BasePage {

    String reviewTabXpath = "//a[@href='#tabs_4']";
    String ratingStarsXpath = "//div[@class='rating ']";
    String sortByDateXpath = "//a[@data-val='date']";
    String sortByBestXpath = "//a[@data-val='rating-desc']";
    String sortByWorstXpath = "//a[@data-val='rating-asc']";
    String addReviewBtnXpath = "//a[@href='#review__Popup']";
    String popupRatingOneXpath = "//label[@for='radio_10004']";
    String popupRatingTwoXpath = "//label[@for='radio_10003']";
    String popupRatingThreeXpath = "//label[@for='radio_10002']";
    String popupRatingFourXpath = "//label[@for='radio_10001']";
    String popupRatingFiveXpath = "//label[@for='radio_10000']";
    String benefitsFieldXpath = "//input[@id='review-popup-form-benefits']";
    String disadvantagesFieldXpath =
            "//input[@id='review-popup-form-disadvantages']";
    String commentFieldXpath = "//textarea[@id='review-popup-form-detail']";
    String nameFieldXpath = "//input[@id='review-popup-form-nickname']";
    String emailFieldXpath = "//input[@id='review-popup-form-email']";
    String receiveAnswerCheckboxXpath = "//input[@id='mycheckbox_8']";
    String submitXpath =
            "//input[@class='button__submit'][@value='Залишити відгук']";
    String successMsgXpath = "//div[@class='popup__body__result success']";
    String commentErrorMsgXpath = "//div[@id='review-popup-form-detail-error']";
    String nameErrorMsgXpath = "//div[@id='review-popup-form-nickname-error']";
    String reviewBlockXpath = "//div[@class='goodsCard__reviews__block']";
    String loadMoreXpath = "//span[contains(text(), 'Показати більше')]";
    String reviewDateXpath = "//div[@class='goodsCard__reviews__block__date']";
    String complaintLinkXpath =
            "//a[@class='goodsCard__reviews__block__complain']";
    String complaintReason1Xpath = "//input[@name='cause[pricina_ua]']";
    String complaintReason2Xpath = "//input[@name='cause[pricina_ua1]']";
    String complaintReason3Xpath =
            "//input[@name='cause[posilanna_na_nezakonni_sajti_i_programi]']";
    String complaintReason4Xpath =
            "//input[@name='cause[ne_stosuet_sa_obgovoruvanoi_temi]']";
    String complaintReason5Xpath = "//input[@name='cause[insa_pricina]']";
    String complaintCommentXpath = "//textarea[@name='description']";
    String complaintSubmitXpath = "//input[@value='Відправити скаргу']";
    String complaintSuccessMsgXpath =
            "//div[@class='popup__body__result success']";
    String averageRatingXpath =
            "//span[@class='goodsCard__reviews__info__num']";

    /**
     * @return count of review blocks on page as int
     */
    public int CountReviewBlocks() {
        try {
            List<WebElement> ratingStars = waitForElements(reviewBlockXpath);
            return ratingStars.size();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Returns rating of product with given serial number
     * @param cardNum serial number of product which rating we need
     * @return product rating as double
     */
    public double GetReviewRate(int cardNum) {
        List<WebElement> ratings =
                driver.findElements(By.xpath(ratingStarsXpath));
        return Double.parseDouble(ratings.get(cardNum).
                getAttribute("data-rating"));
    }

    /**
     * Clicks the 'load more' button on review tab on the product page
     */
    public void LoadMoreReviews() {
        try {
            WebElement loadMoreBtn = waitForElement(loadMoreXpath);
            loadMoreBtn.click();
            System.out.println("Loaded more reviews");
        } catch (Exception e){
            System.out.println("There is no Load more button");
        }
    }

    /**
     *
     * @return count of reviews on review tab on the product page
     */
    public int CountRatingBlocks() {
        List<WebElement> ratingStars = waitForElements(ratingStarsXpath);
        return ratingStars.size();
    }

    /**
     * @return true if there is rating block on the product page
     */
    public boolean IsRatingOnProductPage() {
        WebElement ratingBlock = waitForElement(ratingStarsXpath);
        return ratingBlock.isDisplayed();
    }

    /**
     * Opens review tab on the product page
     */
    public void OpenReviewTab() {
        WebElement reviewTab = waitForElement(reviewTabXpath);
        reviewTab.click();
    }

    /**
     * Opens review popup form on review tab on the product page
     */
    public void OpenReviewForm() {
        WebElement addReviewBtn = waitForElement(addReviewBtnXpath);
        addReviewBtn.click();
    }

    /**
     * Selects sorting by best on review tab on the product page
     */
    public void SortByBest() {
        WebElement sortLink = waitForElement(sortByBestXpath);
        sortLink.click();
    }

    /**
     * Selects sorting by worst on review tab on the product page
     */
    public void SortByWorst() {
        WebElement sortLink = waitForElement(sortByWorstXpath);
        sortLink.click();
    }

    /**
     * Selects sorting by date on review tab on the product page
     */
    public void SortByDate() {
        WebElement sortLink = waitForElement(sortByDateXpath);
        sortLink.click();
    }

    /**
     * Accepts a number from 1 to 5 and sets the corresponding
     * product rating in the review popup
     * @param rating rating we need to set (1-5)
     */
    public void SetRatingReviewInPopup(int rating) {
        WebElement oneStar = waitForElement(popupRatingOneXpath);
        WebElement twoStar = waitForElement(popupRatingTwoXpath);
        WebElement threeStar = waitForElement(popupRatingThreeXpath);
        WebElement fourStar = waitForElement(popupRatingFourXpath);
        WebElement fiveStar = waitForElement(popupRatingFiveXpath);

        switch (rating) {
            case 1 -> oneStar.click();
            case 2 -> twoStar.click();
            case 3 -> threeStar.click();
            case 4 -> fourStar.click();
            case 5 -> fiveStar.click();
        }
    }

    /**
     * Sends data to benefits field on the review popup
     * @param benefit data to send to benefits field
     */
    public void SendBenefits (String benefit) {
        WebElement benefitsField = waitForElement(benefitsFieldXpath);
        benefitsField.sendKeys(benefit);
    }

    /**
     * Sends data to disadvantage field on the review popup
     * @param disadvantage data to send to disadvantage field
     */
    public void SendDisadvantages (String disadvantage) {
        WebElement disadvantageField =
                waitForElement(disadvantagesFieldXpath);
        disadvantageField.sendKeys(disadvantage);
    }

    /**
     * Sends data to comment field on the review popup
     * @param comment data to send to comment field
     */
    public void SendComment (String comment) {
        WebElement commentField = waitForElement(commentFieldXpath);
        commentField.sendKeys(comment);
    }

    /**
     * Sends data to name field on the review popup
     * @param name data to send to name field
     */
    public void SendName (String name) {
        WebElement nameField = waitForElement(nameFieldXpath);
        nameField.sendKeys(name);
    }

    /**
     * Sends data to email field on the review popup
     * @param email data to send to email field
     */
    public void SendEmail (String email) {
        WebElement emailField = waitForElement(emailFieldXpath);
        emailField.sendKeys(email);
    }

    /**
     * Clicks on receive answers checkbox
     */
    public void CheckReceiveAnswer() {
        WebElement checkbox = waitForElement(receiveAnswerCheckboxXpath);
        checkbox.click();
    }

    /**
     * Clicks submit button on review popup
     */
    public void SubmitReview() {
        WebElement submitBtn = waitForElement(submitXpath);
        submitBtn.click();
    }

    /**
     * @return true if review sent, and there is the success message
     */
    public boolean IsSuccessMsg() {
        return waitForElement(successMsgXpath).isDisplayed();
    }

    /**
     * @return true if there is an error message below the comment field
     */
    public boolean IsCommentErrorMsg() {
        return waitForElement(commentErrorMsgXpath).isDisplayed();
    }

    /**
     * @return true if there is an error message below the name field
     */
    public boolean IsNameErrorMsg() {
        return waitForElement(nameErrorMsgXpath).isDisplayed();
    }

    /**
     * Accepts string with date, and convert it to Date format
     * @param dateIn date as string in format "20 січня 2021р"
     * @return converted date in Date format
     */
    public Date ConvertStringToDate(String dateIn) throws ParseException {
        String dateRemovedSpace = dateIn.replace(" ", "/");
        String dateStr = dateRemovedSpace.substring(0,
                dateRemovedSpace.length()-1);

        if (dateStr.contains("січня")) {
            dateStr = dateStr.replace("січня", "01");
        } else if (dateStr.contains("лютого")) {
            dateStr = dateStr.replace("лютого", "02");
        } else if (dateStr.contains("березня")) {
            dateStr = dateStr.replace("березня", "03");
        } else if (dateStr.contains("квітня")) {
            dateStr = dateStr.replace("квітня", "04");
        } else if (dateStr.contains( "травня")) {
            dateStr = dateStr.replace( "травня", "05");
        } else if (dateStr.contains("червня")) {
            dateStr = dateStr.replace("червня", "06");
        } else if (dateStr.contains("липня")) {
            dateStr = dateStr.replace("липня", "07");
        } else if (dateStr.contains("серпня")) {
            dateStr = dateStr.replace("серпня", "08");
        } else if (dateStr.contains("вересня")) {
            dateStr = dateStr.replace("вересня", "09");
        } else if (dateStr.contains("жовтня")) {
            dateStr = dateStr.replace("жовтня", "10");
        } else if (dateStr.contains("листопада")) {
            dateStr = dateStr.replace("листопада", "11");
        } else if (dateStr.contains("грудня")) {
            dateStr = dateStr.replace("грудня", "12");
        }
        return new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
    }

    //Returns Date of review with given serial number

    /**
     *
     * @param reviewNum serial number of review, from
     *                  which we need to return a date
     * @return date of review in Date format
     */
    public Date GetReviewDate(int reviewNum) throws ParseException {
        List<WebElement> dates = waitForElements(reviewDateXpath);
        return ConvertStringToDate(dates.get(reviewNum).getText());
    }

    /**
     * Opens a pop-up with a review complaint form
     * @param reviewNum serial number of review we need to complaint to
     */
    public void OpenComplaintForm(int reviewNum) {
        List<WebElement> complaintLinks = waitForElements(complaintLinkXpath);
        complaintLinks.get(reviewNum).click();
    }

    //Accepts number of complaint reason and marks the checkbox of the
    // reasons for the complaint
    //Accepts numbers from 0 to 5. 1-5 — numbers of reasons.
    // 0 — selects all checkboxes

    /**
     * Marks the checkbox of the reasons for the complaint
     * @param reasonNum serial number of complaint reason we need to select
     *                  Accepts numbers from 0 to 5. 1-5 — numbers of reasons.
     *                  0 — selects all checkboxes
     */
    public void SetComplaintReason(int reasonNum) {
        WebElement firstReason = waitForElement(complaintReason1Xpath);
        WebElement secondReason = waitForElement(complaintReason2Xpath);
        WebElement thirdReason = waitForElement(complaintReason3Xpath);
        WebElement fourthReason = waitForElement(complaintReason4Xpath);
        WebElement fifthReason = waitForElement(complaintReason5Xpath);

        switch (reasonNum) {
            case 1 -> firstReason.click();
            case 2 -> secondReason.click();
            case 3 -> thirdReason.click();
            case 4 -> fourthReason.click();
            case 5 -> fifthReason.click();
            case 0 -> {
                secondReason.click();
                thirdReason.click();
                fourthReason.click();
                fifthReason.click();
            }
        }
    }

    /**
     * Send data to comment field in complaint form
     * @param comment data to send to comment field
     */
    public void SendComplaintComment(String comment) {
        WebElement commentField = waitForElement(complaintCommentXpath);
        commentField.sendKeys(comment);
    }

    /**
     * Clicks submit button in complaint form
     */
    public void SubmitComplaint() {
        WebElement submitComplaint = waitForElement(complaintSubmitXpath);
        submitComplaint.click();
    }

    /**
     *
     * @return true if complaint sent and there is a success message
     */
    public boolean IsComplaintSuccess() {
        WebElement successMsg = waitForElement(complaintSuccessMsgXpath);
        return successMsg.isDisplayed();
    }

    /**
     * @return string with average product rating
     */
    public String GetAverageRating() {
        WebElement averageRating = waitForElement(averageRatingXpath);
        String ratingStr = averageRating.getText();
        int indexOfDot = ratingStr.indexOf(".");

        if (indexOfDot == -1) {
            return averageRating.getText() + ".0";
        } else {
            return averageRating.getText();
        }
    }

}
