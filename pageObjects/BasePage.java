package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Driver.WedDriver;

import java.util.List;


public class BasePage extends WedDriver {

    //Defining Variables
    public String productCardXpath =
            "//div[@class = 'goodsPreview__item__miniature']";
    public String searchMenuXpath =
            "//div[contains(@class, 'header__interface__search')]";
    public String cartMenuXpath =
            "//a[contains(@class, 'header__interface__cart')]";
    public String compareMenuXpath =
            "//a[contains(@class, 'header__interface__compare')]";
    public String gasMenuXpath =
            "//a[@class='header__nav__item'][contains(text(), 'Газові котли')]";
    public String boilerMenuXpath =
            "//a[@class='header__nav__item'][contains(text(), 'Водонагрівачі')]";
    public String countersMenuXpath =
            "//a[@class='header__nav__item'][contains(text(), 'Лічильники')]";
    public String othersMenuXpath =
            "//a[@class='header__nav__item'][contains(text(), 'Інше')]";
    public String searchFieldXpath = "//input[@id='search-input']";
    public String searchBtnXpath =
            "//button[contains(@class, 'search-popup__submit')]";
    public String searchResultXpath =
            "//span[contains(@class, 'search-popup__result-item-text')]" +
                    "[contains(text(), 'Котел')]";
    public String noResultMessageXpath =
            "//p[contains(text(), 'За вашим запитом нічого не знайдено')]";
    public String wishMenuXpath =
            "//a[contains(@class, 'header__interface__item " +
                    "header__interface__chosen')]";
    public String continueShoppingBtnXpath =
            "//span[text() = 'Продовжити покупки']";
    public String addToCartBtnXpath =
        "//span[contains(@class, 'button__addToCart')]";
    public String addToCartProductPage =
            "//span[contains(@class, 'button__addToCart')]";
    public String removeFromCartXpath = "//button[@class='delete-btn']";
    public String emptyCartMessageXpath = "//p[@class='empty-page-message']";
    public String amountInCartXpath = "//input[@class = 'amount']";
    public String amountMinusXpath = "//span[@class='minus']";
    public String amountPlusXpath = "//span[@class='plus']";
    public String sumInCartXpath = "//span[@data-cart-item-price='']";
    public String checkoutBtnXpath = "//span[text() = 'Оформити замовлення']";
    public String checkoutBtnCartPageXpath = "//a[text()='Оформити замовлення']";
    public String characteristicTabXpath = "//a[@data-tab='1']";


    /**
     * @return current url in String format
     */
    public String GetCurrentUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * Waits for 20 sec until Web element will be visible
     * @param locator xpath of element we looking for
     * @return web element if it's visible
     */
    public WebElement waitForElement(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.
                visibilityOfElementLocated(By.xpath(locator)));
    }

    /**
     * Waits for 10 sec until Web elements will be visible
     * @param locator xpath of elements we looking for
     * @return list of web elements if they are visible
     */
    public List<WebElement> waitForElements(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return wait.until(ExpectedConditions.
                visibilityOfAllElementsLocatedBy(By.xpath(locator)));
    }

    /**
     * Waits for 10 sec until Web element will be present
     * @param locator xpath of elements we looking for
     * @return list of web elements if they are present
     */
    public List<WebElement> waitForElementsPresent(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(By.xpath(locator)));
    }

    /**
     * Sends search request to search field and click search button
     * @param searchRequest String with search request
     */
    public void sendSearchRequest(String searchRequest) {
        WebElement searchMenu = driver.findElementByXPath(searchMenuXpath);
        WebElement searchField = driver.findElementByXPath(searchFieldXpath);
        WebElement searchBtn = driver.findElementByXPath(searchBtnXpath);

        searchMenu.click();
        searchField.sendKeys(searchRequest);
        searchBtn.click();
    }

    /**
     * Opens a product page by clicks on the product card with a specified number
     * @param cardNum serial number of product on the page, begin from 0
     */
    public void OpenProductNum(int cardNum) {
        List<WebElement> productCards = waitForElements(productCardXpath);
        productCards.get(cardNum).click();
    }

    /**
     * Opens characteristics tab on the product page
     */
    public void OpenCharacteristicTab() {
        WebElement characteristicTab = waitForElement(characteristicTabXpath);
        characteristicTab.click();
    }

    /**
     * Opens Gas category page
     */
    public void GoToGasCategory() {
//        WebElement gasMenuItem = waitForElement(gasMenuXpath);
        WebElement gasMenuItem = waitForElement(gasMenuXpath);
        gasMenuItem.click();
    }

    /**
     * Opens Boiler category page
     */
    public void GoToBoilerCategory() {
        WebElement gasMenuItem = waitForElement(boilerMenuXpath);
        gasMenuItem.click();
    }
    /**
     * Opens Counters category page
     */
    public void GoToCountersCategory() {
        WebElement gasMenuItem = waitForElement(countersMenuXpath);
        gasMenuItem.click();
    }

    /**
     * Opens Other category page
     */
    public void GoToOthersCategory() {
        WebElement gasMenuItem = waitForElement(othersMenuXpath);
        gasMenuItem.click();
    }

    /**
     * Opens Wishlist page
     */
    public void GoToWishlist() {
        WebElement wishMenuItem = waitForElement(wishMenuXpath);
        wishMenuItem.click();
    }

    /**
     * Opens Compare page
     */
    public void GoToCompare() {
        WebElement compareMenuItem = waitForElement(compareMenuXpath);
        compareMenuItem.click();
    }

    /**
     * Opens cart popup
     */
    public void OpenCart() {
        WebElement cartMenuItem = waitForElement(cartMenuXpath);
        cartMenuItem.click();
    }

    /**
     * Opens address: base page + given url
     * @param url endpoint
     */
    public void OpenUrl(String url) {
        driver.get(base_page + url);
    }

    /**
     * Closes cart pop-up
     */
    public void ContinueShoppingCart() {
        WebElement closeCartBtn = waitForElement(continueShoppingBtnXpath);
        closeCartBtn.click();
    }

    /**
     * Opens checkout page from cart
     */
    public void GoToCheckoutFromCart() {
        WebElement checkout = waitForElement(checkoutBtnXpath);
        checkout.click();
    }

    /**
     * Opens checkout page from the cart page
     */
    public void GoToCheckout() {
        WebElement checkout = waitForElement(checkoutBtnCartPageXpath);
        checkout.click();
    }

    /**
     * Adds product to cart
     * @param num serial number of the product we want to add to the cart
     */
    public void AddToCart(int num) {
        List<WebElement> adToCartBtns = waitForElements(addToCartBtnXpath);
        adToCartBtns.get(num).click();
    }

    /**
     * Adds product to cart from product page
     */
    public void AddToCartProductPage() {
        WebElement adToCartBtn = waitForElement(addToCartProductPage);
        adToCartBtn.click();
    }

    /**
     * Removes product with given serial number from cart popup
     * @param num serial number of the product we want to remove from the cart
     */
    public void RemoveFromCart(int num) {
        List<WebElement> removeBtn = waitForElements(removeFromCartXpath);
        removeBtn.get(num).click();
    }

    /**
     * @return true, if cart is empty
     */
    public boolean IsEmptyCart() {
        WebElement emptyMessage = waitForElement(emptyCartMessageXpath);
        return emptyMessage.isDisplayed();
    }

    /**
     * Count products in cart popup (how many products and items of each),
     * and compare it with given numbers.
     * @param number number of items for each product
     * @param row count of products in cart
     * @return true if the number of products is matched with given
     */
    public boolean CheckProductCountInCart(int number, int row) {
        List<WebElement> cartProductsCountXpath =
               waitForElements
                       ("//input[contains(@class, 'amount') " +
                               "and contains(@value, '"
                                + number + "')]");
        return cartProductsCountXpath.get(row).isDisplayed();
    }

    /**
     * Changes amount of product in cart popup
     * @param amount +1 — clicks plus btn
     *               -1 — clicks minus btn
     *               1-999 — set number of items in the amount field
     */
    public void ChangeAmountCart(int amount) {
        WebElement plus = waitForElement(amountPlusXpath);
        WebElement minus = waitForElement(amountMinusXpath);
        WebElement amountInput = waitForElement(amountInCartXpath);

        if (amount == +1){
            plus.click();
        } else if (amount == -1) {
            minus.click();
        } else {
            amountInput.sendKeys(Integer.toString(amount));
        }
    }

    /**
     * @return displayed cost in the cart popup
     */
    public int GetSumInCart() {
        WebElement sumField = waitForElement(sumInCartXpath);
        String sum = sumField.getText();
        return Integer.parseInt(sum.replace(" ", ""));
    }

    /**
     * @return number of products on page
     */
    public int CountProducts() {
        List<WebElement> ratings = waitForElements(productCardXpath);
        return ratings.size();
    }

}
