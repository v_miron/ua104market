package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class SortFilterPage extends pageObjects.BasePage {

    String popularXpath = "//a[@data-val='popular']";
    String priceAscXpath = "//a[@data-val='price-asc']";
    String priceDescXpath = "//a[@data-val='price-desc']";
    String newFirstXpath = "//a[@data-val='new']";
    String ratingXpath = "//a[@data-val='rating']";
    String showMoreBtn = "//span[contains(text(), 'Показати більше')]";
    String ratingStarsXpath = "//div[@class='rating ']";
    String priceXpath = "//div[@class='goodsPreview__item__price']";
    String priceStartXpath = "//input[@class='filter-price__range-start']";
    String priceFinishXpath = "//input[@class='filter-price__range-end']";
    String menuUnusedXpath = "//div[@class='header__bottom__inner']";
    String newLabelXpath =
            "//span[@class='goodsPreview__item__label newGoods']";
    String productCardXpath =
            "//div[@class = 'goodsPreview__item__miniature']";
    String powerFilter24Xpath =
            "//input[@id='potuzhnist_nominal_na_kvt_21032']";
    String power24Xpath = "//div[2]/section/div[1]/div[3]/div[2]/div/a";
    String controlMechanicalXpath = "//input[@id='upravlinnja_22171']";
    String controlXpath = "//a[@href='/ua/products/c19?upravlinnja=22171']";
    String materialSteelXpath =
            "//input[@id='material_teploobminnika_gvp_21959']";
    String steelXpath =
            "//a[@href='/ua/products/c19?material_teploobminnika_gvp=21959']";

    /**
     * Checks 'new' labels on product card with given serial number
     * @param cardNum serail number of product we need to check
     * @return true if there is new label on current product
     */
    public boolean CheckNewLabel (int cardNum) {
        List<WebElement> newLabels =
                driver.findElements(By.xpath(newLabelXpath));

        if (cardNum < newLabels.size()) {
            return newLabels.get(cardNum).isDisplayed();
        } else {
            return false;
        }
    }

    /**
     * Selects sorting by popular
     */
    public void SortByPopular() {
        WebElement popularBtn = waitForElement(popularXpath);
        popularBtn.click();
    }

    /**
     * Selects sorting by ascending price
     */
    public void SortPriceAsc() {
        WebElement priceAscBtn = waitForElement(priceAscXpath);
        priceAscBtn.click();
    }

    /**
     * Selects sorting by descending price
     */
    public void SortPriceDesc() {
        WebElement priceDescBtn = waitForElement(priceDescXpath);
        priceDescBtn.click();
    }

    /**
     * Selects sorting by new first
     */
    public void SortNewFirst() {
        WebElement newFirstBtn = waitForElement(newFirstXpath);
        newFirstBtn.click();
    }

    /**
     * Selects sorting by rating
     */
    public void SortByRating() {
        WebElement ratingBtn = waitForElement(ratingXpath);
        ratingBtn.click();
    }

    /**
     * Clicks load more button
     */
    public void LoadMore() {
        WebElement showMore = waitForElement(showMoreBtn);

        if (showMore != null) {
            showMore.click();
        }
    }

    /**
     * @return number of products on page
     */
    public int CountProducts() {
        List<WebElement> ratings = waitForElements(productCardXpath);
        return ratings.size();
    }

    /**
     * Returns rating of product with given serial number
     * @param cardNum serial number of product which rating we need to get
     * @return rating of current product as double
     */
    public double GetCardRate(int cardNum) {
        List<WebElement> ratings =
                driver.findElements(By.xpath(ratingStarsXpath));
        return Double.parseDouble(ratings.get(cardNum).
                getAttribute("data-rating"));
    }

    /**
     * Returns price of product with given serial number
     * @param cardNum serial number of product which price we need to get
     * @return price of current product as int
     */
    public int GetCardPrice(int cardNum) {
        List<WebElement> price =
                driver.findElements(By.xpath(priceXpath));
        String fullText = price.get(cardNum).getText();
        fullText = fullText.substring(0, fullText.length() - 4);
        fullText = fullText.replace(" ", "");
        return Integer.parseInt(fullText);
    }

    /**
     * Sets price range on the category page, than wait for result for 20 seconds
     * @param start min price
     * @param finish max price
     */
    public void SetPriceRange(String start, String finish)
            throws InterruptedException {
        WebElement priceStart = waitForElement(priceStartXpath);
        WebElement priceFinish = waitForElement(priceFinishXpath);
        WebElement menu = waitForElement(menuUnusedXpath);
        priceStart.clear();
        priceStart.sendKeys(start);
        priceFinish.clear();
        priceFinish.sendKeys(finish);
        menu.click();
        Thread.sleep(20000);
    }

    /**
     * Selects filters: power, control and material
     */
    public void SelectFilters() throws InterruptedException {
        WebElement power = waitForElement(powerFilter24Xpath);
        WebElement control = waitForElement(controlMechanicalXpath);
        WebElement material = waitForElement(materialSteelXpath);
        Actions act = new Actions(driver);
        act.moveToElement(power).perform();
        power.click();
        Thread.sleep(6000);
        act.moveToElement(control).perform();
        control.click();
        Thread.sleep(6000);
        act.moveToElement(material).perform();
        material.click();
        Thread.sleep(6000);
    }

    /**
     *
     * @return true if characteristics on product page matches
     * characteristics we selected in SelectFilters()
     */
    public boolean IsCorrectCharacteristic() {
        WebElement power = waitForElement(power24Xpath);
        WebElement control = waitForElement(controlXpath);
        WebElement material = waitForElement(steelXpath);

        return power.isDisplayed() && control.isDisplayed()
                && material.isDisplayed();
    }

}
