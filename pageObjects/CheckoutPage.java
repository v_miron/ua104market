package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class CheckoutPage extends pageObjects.BasePage {

    String lastNameXpath = "//input[@name='last_name']";
    String firstNameXpath = "//input[@name='first_name']";
    String middleNameXpath = "//input[@name='middle_name']";
    String phoneXpath = "//input[@name='tel']";
    String sameRecipientXpath = "//label[@for='same-value']";
    String otherRecipientXpath = "//label[@for='not-same-value']";
    String recipientLastNameXpath = "//input[@name='recipient_lastname']";
    String recipientFirstNameXpath = "//input[@name='recipient_firstname']";
    String recipientMiddleNameXpath =
            "//input[@name='recipient_middlename']";
    String recipientPhoneXpath = "//input[@name='recipient_tel']";
    String personalDataXpath = "//input[@name='personalData']";
    String pageXpath = "//main[@class='checkout-page']";
    String submitBtnXpath = "//input[@type='submit']";
    String paymentUrlPath = "checkout.ipay.ua";
    String postRegionXpath = "//select[@id='location-region']";
    String postCityXpath = "//select[@id='location-city']";
    String postWarehouseXpath =
            "//select[@id='newposhta_newposhta_del_address']";
    String errorMessageXpath = "//div[@class='error']";
    String lastNameErrorXpath = "//div[@id='input-1-error']";
    String firstNameErrorXpath = "//div[@id='input-2-error']";
    String middleNameErrorXpath = "//div[@id='input-3-error']";
    String phoneErrorXpath = "//div[@id='input-4-error']";
    String recipientLastNameErrorXpath = "//div[@id='input-8-error']";
    String recipientFirstNameErrorXpath = "//div[@id='input-9-error']";
    String recipientMiddleNameErrorXpath = "//div[@id='input-10-error']";
    String recipientPhoneErrorXpath = "//div[@id='input-11-error']";
    String personalDataErrorXpath = "//div[@id='personalData-error']";
    public String deliveryPriceXpath =
            "//div[@data-checkout-delivery-price='']";

    /**
     * Selects delivery region by given index
     * @param index of region to select
     */
    public void SetDeliveryRegion (int index) {
        WebElement postRegion = waitForElement(postRegionXpath);
        Select selectRegion = new Select(postRegion);
        selectRegion.selectByIndex(index);
    }

    /**
     * Selects delivery city by given index
     * @param index of city to select
     */
    public void SetDeliveryCity (int index) {
        WebElement postCity = waitForElement(postCityXpath);
        Select selectCity = new Select(postCity);
        selectCity.selectByIndex(index);
    }

    /**
     * Selects delivery warehouse by given index
     * @param index of warehouse to select
     */
    public void SetDeliveryWarehouse (int index) {
        WebElement postWarehouse = waitForElement(postWarehouseXpath);
        Select selectWarehouse = new Select(postWarehouse);
        selectWarehouse.selectByIndex(index);
    }

    /**
     * @return true if user got to the payment page
     */
    public boolean IsPaymentPage() {
        String currentUrl = driver.getCurrentUrl();
        int indexOfUrl = currentUrl.indexOf(paymentUrlPath);
        return indexOfUrl != -1;
    }

    /**
     * @return true if there any error message
     */
    public boolean IsErrorMessage() {
        WebElement errorMessage = waitForElement(errorMessageXpath);
        return errorMessage.isDisplayed();
    }

    /**
     * @return true if there last name field error message
     */
    public boolean IsErrorLastName() {
        WebElement lastNameError = waitForElement(lastNameErrorXpath);
        return lastNameError.isDisplayed();
    }

    /**
     * @return true if there first name field error message
     */
    public boolean IsErrorFirstName() {
        WebElement firstNameError = waitForElement(firstNameErrorXpath);
        return firstNameError.isDisplayed();
    }

    /**
     * @return true if there middle name field error message
     */
    public boolean IsErrorMiddleName() {
        WebElement middleNameError = waitForElement(middleNameErrorXpath);
        return middleNameError.isDisplayed();
    }

    /**
     * @return true if there phone field error message
     */
    public boolean IsErrorPhone() {
        WebElement phoneError = waitForElement(phoneErrorXpath);
        return phoneError.isDisplayed();
    }

    /**
     * @return true if there recipient last name field error message
     */
    public boolean IsErrorRecipientLastName() {
        WebElement recipientLastNameError =
                waitForElement(recipientLastNameErrorXpath);
        return recipientLastNameError.isDisplayed();
    }

    /**
     * @return true if there recipient first name field error message
     */
    public boolean IsErrorRecipientFirstName() {
        WebElement recipientFirstNameError =
                waitForElement(recipientFirstNameErrorXpath);
        return recipientFirstNameError.isDisplayed();
    }

    /**
     * @return true if there recipient middle name filed error message
     */
    public boolean IsErrorRecipientMiddleName() {
        WebElement recipientMiddleNameError =
                waitForElement(recipientMiddleNameErrorXpath);
        return recipientMiddleNameError.isDisplayed();
    }

    /**
     * @return true if there recipient phone field error message
     */
    public boolean IsErrorRecipientPhone() {
        WebElement recipientPhoneError =
                waitForElement(recipientPhoneErrorXpath);
        return recipientPhoneError.isDisplayed();
    }

    /**
     * @return true if there personal data error message
     */
    public boolean IsPersonalDataErrorMessage() {
        WebElement pdErrorMessage =
                waitForElement(personalDataErrorXpath);
        return pdErrorMessage.isDisplayed();
    }

    /**
     * Sends data to last name field
     * @param lastName data to send
     * if lastName equal to "clear" - it's clear last name field
     */
    public void SendLastName (String lastName) {
        WebElement lastNameField = waitForElement(lastNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (lastName.equals("clear")) {
            lastNameField.clear();
        } else {
            lastNameField.sendKeys(lastName);
            page.click();
        }
    }

    /**
     * Sends data to first name field
     * @param name data to send
     * if name equal to "clear" - it's clear name field
     */
    public void SendName (String name) {
        WebElement nameField = waitForElement(firstNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (name.equals("clear")) {
            nameField.clear();
        } else {
            nameField.sendKeys(name);
            page.click();
        }
    }

    /**
     * Sends data to middle name field
     * @param middleName data to send
     * if middleName equal to "clear" - it's clear last name field
     */
    public void SendMiddleName (String middleName) {
        WebElement middleNameField = waitForElement(middleNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (middleName.equals("clear")) {
            middleNameField.clear();
        } else {
            middleNameField.sendKeys(middleName);
            page.click();
        }
    }

    /**
     * Sends data to phone field
     * @param phone data to send
     */
    public void SendPhone (String phone) {
        WebElement phoneField = waitForElement(phoneXpath);
        WebElement page = driver.findElementByXPath(pageXpath);
        phoneField.sendKeys(phone);
        page.click();
    }

    /**
     * Selects recipient
     * @param recipient if equals 'same', selects same recipient
     *                  if equals 'other', selects other recipient
     */
    public void SelectRecipient (String recipient) {
        WebElement sameRecipient = driver.findElement
                (By.xpath(sameRecipientXpath));
        WebElement otherRecipient = driver.findElement
                (By.xpath(otherRecipientXpath));

        if (recipient.equals("same")) {
            if (!sameRecipient.isSelected()){
                sameRecipient.click();
            }
        } else if (recipient.equals("other")) {
            if(!otherRecipient.isSelected()){
                otherRecipient.click();
            }
        }
    }

    /**
     * Sends data to recipient last name field
     * @param recipientLastName data to send
     * if recipientLastName equal to "clear" - it's clear last name field
     */
    public void SendRecipientLastName (String recipientLastName) {
        WebElement recipientLastNameField =
                waitForElement(recipientLastNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (recipientLastName.equals("clear")) {
            recipientLastNameField.clear();
        } else {
            recipientLastNameField.sendKeys(recipientLastName);
            page.click();
        }
    }

    /**
     * Sends data to recipient name field
     * @param recipientName data to send
     * if recipientName equal to "clear" - it's clear last name field
     */
    public void SendRecipientName (String recipientName) {
        WebElement recipientNameField =
                waitForElement(recipientFirstNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (recipientName.equals("clear")) {
            recipientNameField.clear();
        } else {
            recipientNameField.sendKeys(recipientName);
            page.click();
        }
    }

    /**
     * Sends data to recipient middle name field
     * @param recipientMiddleName data to send
     * if recipientMiddleName equal to "clear" - it's clear last name field
     */
    public void SendRecipientMiddleName (String recipientMiddleName) {
        WebElement recipientMiddleNameField =
                waitForElement(recipientMiddleNameXpath);
        WebElement page = driver.findElementByXPath(pageXpath);

        if (recipientMiddleName.equals("clear")) {
            recipientMiddleNameField.clear();
        } else {
            recipientMiddleNameField.sendKeys(recipientMiddleName);
            page.click();
        }
    }

    //Sends data to recipient phone field
    /**
     * Sends data to recipient phone field
     * @param recipientPhone data to send
     */
    public void SendRecipientPhone (String recipientPhone) {
        WebElement recipientPhoneField =
                waitForElement(recipientPhoneXpath);
        WebElement page = driver.findElementByXPath(pageXpath);
        recipientPhoneField.sendKeys(recipientPhone);
        page.click();
    }

    /**
     * Selects checkbox of personal data processing
     */
    public void PersonalDataAccept () {
        WebElement pdCheckbox = waitForElement(personalDataXpath);
        boolean isAccepted = pdCheckbox.isSelected();
        if (!isAccepted) {
            pdCheckbox.click();
        }
    }

    /**
     * Removes select from checkbox of personal data processing
     */
    public void PersonalDataDoNotAccept () {
        WebElement pdCheckbox = waitForElement(personalDataXpath);
        boolean isAccepted = pdCheckbox.isSelected();
        if (isAccepted) {
            pdCheckbox.click();
        }
    }

    /**
     * Click submit button
     */
    public void Submit() {
        WebElement submit = waitForElement(submitBtnXpath);
        submit.click();
    }

    /**
     * @return delivery price as String
     */
    public String GetDeliveryPrice() {
        WebElement price = waitForElement(deliveryPriceXpath);
        return price.getText().replace(" грн", "");
    }

}
