package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class ComparePage extends pageObjects.BasePage {

    String addToCompareBtnXpath = "//span[contains(@class, 'addCompare')]";
    String addToCompareTopBtnXpath = "//a[contains(@class, 'addCompare')]";
    String compareItemXpath = "//div[@class='compare__item__top']";
    String removeBtnXpath = "//a[@class='compare__item__close']";
    String removeAllBtnXpath = "//a[@class='compare__closeLink']";
    String emptyCompareXpath = "//p[@class='empty-page-message']";
    String nextItemBtn = "//div[@class='swiper-button-next']";
    String prevItemBtn = "//div[@class='swiper-button-prev']";
    String onlyDiffsFilterXpath =
            "//label[contains(text(), 'Тільки відмінності')]";
    String characteristicRowXpath = "//div[@class='compare__block__row']";


    /**
     * Selects only difference filter
     */
    public void SelectOnlyDiffsFilter(){
        WebElement diffsFilter = waitForElement(onlyDiffsFilterXpath);
        diffsFilter.click();
    }

    /**
     * @param productNum serial number of product, to count characteristic
     * @return count of characteristic rows
     */
    public int CountCharacteristicRows (int productNum) {
        List<WebElement> row =
                driver.findElements(By.xpath(characteristicRowXpath));
        return row.size() / productNum;
    }

    /**
     * @return count of compare blocks on the page
     */
    public int CountCompareBlocks() {
        List<WebElement> products = waitForElements(productCardXpath);
        List<WebElement> btns = waitForElementsPresent(addToCompareBtnXpath);
        int counter = 0;
        Actions act = new Actions(driver);

        for (int i=0; i < products.size(); i++) {
            act.moveToElement(products.get(i)).perform();
            boolean isCompareBtns = btns.get(i).isDisplayed();
                if (isCompareBtns) {
                    counter += 1;
                }
            }
        return counter;
    }

    /**
     * @return true if on the page present two compare
     * buttons (top and sidebar buttons)
     */
    public boolean IsCompareBtnProductPage() {
        WebElement topBtn = waitForElement(addToCompareTopBtnXpath);
        WebElement sideBtn = waitForElement(addToCompareBtnXpath);
        return topBtn.isDisplayed() && sideBtn.isDisplayed();
    }

    /**
     * Adds a product to the compare list
     * @param cardNum serial number of product to add to compare
     */
    public void AddToCompareCardNum(int cardNum) {
        Actions act = new Actions(driver);

        List<WebElement> productCards =
                driver.findElements(By.xpath(productCardXpath));
        act.moveToElement(productCards.get(cardNum));
        act.moveToElement(productCards.get(cardNum)).perform();

        List<WebElement> addToCompareBtn =
                driver.findElements(By.xpath(addToCompareBtnXpath));
        addToCompareBtn.get(cardNum).click();
    }

    /**
     * On product page adds current product to compare list
     */
    public void AddToCompFromTopProdPage() {
        WebElement addToCompBtn = waitForElement(addToCompareTopBtnXpath);
        addToCompBtn.click();
    }

    /**
     * Adds a product to the compare list from the sidebar on product page
     */
    public void AddToCompFromSideProdPage() {
        WebElement addToCompBtn = waitForElement(addToCompareBtnXpath);
        addToCompBtn.click();
    }

    //Returns true if the number of products
    // in the compare list matches the given number

    /**
     * @return true if the number of products in the compare
     * list matches the given number in param numOfItems
     */
    public boolean CountComparedItems(int numOfItems){
        List<WebElement> compareItems =
                driver.findElements(By.xpath(compareItemXpath));
        return numOfItems == compareItems.size();
    }

    /**
     * Removes item from the compare list
     * @param cardNum serial number of item to remove from compare list
     */
    public void RemoveCardNum(int cardNum) {
        List<WebElement> removeBtn =
                driver.findElements(By.xpath(removeBtnXpath));
        removeBtn.get(cardNum).click();
    }

    /**
     * Removes all items from compare list
     */
    public void RemoveAll() {
        WebElement removeAllBtn = waitForElement(removeAllBtnXpath);
        removeAllBtn.click();
    }

    /**
     * @return true if compare list is empty, or it contains only one item
     */
    public boolean isEmptyCompare() {
        WebElement message = waitForElement(emptyCompareXpath);
        return message.isDisplayed();
    }

    /**
     * Compare the number near Cart menu icon with given number
     * @param num number to compare
     * @return true if given number and the number
     * near Cart menu icon is matches
     */
    public boolean MenuCartCounter(int num) {
        WebElement cartCounter = waitForElement("//a[contains(@class, " +
                "'header__interface__cart')]//span[contains(text(), '"
                + num + "')]");
        return cartCounter.isDisplayed();
    }

    /**
     * Clicks the right arrow in compare list
     */
    public void NavigateNextItem() {
        WebElement nextBtn = waitForElement(nextItemBtn);
        nextBtn.click();
    }

    /**
     * Clicks the left arrow in compare list
     */
    public void NavigatePrevItem() {
        WebElement prevBtn = waitForElement(prevItemBtn);
        prevBtn.click();
    }
}
