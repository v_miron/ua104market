package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class WishlistPage extends pageObjects.BasePage {

    public String productCardXpath =
            "//div[@class = 'goodsPreview__item__miniature']";
    public String addToWishBtnXpath =
            "//span[contains(@class, 'addFavourite ')]";
    public String addToWishBtnProductPageXpath =
            "//a[contains(@class, 'addFavourite')]";
    public String addToWishRightBtnProductPageXpath =
            "//span[contains(@class, " +
                    "'goodsCard__order__add__item addFavourite')]";
    public String firstCardTitleXpath =
            "//a[contains(@href, '/ua/product/')]";
    public String removeProductFromWishBtnXpath =
            "//a[@class='compare__item__close']";
    public String removeAllFromWishBtnXpath =
            "//a[@class='compare__closeLink']";
    public String emptyWishMessageXpath =
            "//div[contains(@class, 'elected-empty-info')]";
    public String addToCartBtnXpath =
            "//span[contains(@class, 'button__addToCart')]";
    public String addToCartCheckboxXpath =
            "//input[contains(@class, 'main__checkbox')]";
    public String addToCartCheckedXpath =
            "//a[@class='button__main elected__buy_button']";
    public String checkAllCheckboxXpath =
            "//label[contains(@class, 'main__checkbox__label') " +
                    "and contains(text(), 'Вибрати всі')]";

    /**
     * Adds a product to the wishlist by its serial number in the category
     * @param cardNum serial number of product
     */
    public void AddToWishCardNum(int cardNum){
        Actions act = new Actions(driver);

        List<WebElement> productCards = waitForElements(productCardXpath);
        act.moveToElement(productCards.get(cardNum)).perform();

        List<WebElement> addToWishBtn =
                driver.findElements(By.xpath(addToWishBtnXpath));
        addToWishBtn.get(cardNum).click();
    }

    /**
     * Checks a product checkbox in the wishlist
     * @param cardNum serial number of product
     */
    public void CheckProductCheckboxNum(int cardNum){
        List<WebElement> addToCartCheckbox =
                driver.findElements(By.xpath(addToCartCheckboxXpath));
        addToCartCheckbox.get(cardNum).click();
    }

    /**
     * Selects the checkboxes of all the products in the wishlist
     */
    public void CheckAllCheckbox(){
        WebElement checkAll = waitForElement(checkAllCheckboxXpath);
        checkAll.click();
    }

    /**
     * Adds a product to the wishlist from the product page
     */
    public void AddToWishProductPage() {
        WebElement addBtn = waitForElement(addToWishBtnProductPageXpath);
        addBtn.click();
    }

    /**
     * Adds a product to the wishlist from the sidebar on the product page
     */
    public void AddToWishProductPageRightBtn() {
        WebElement addBtn = waitForElement(addToWishRightBtnProductPageXpath);
        addBtn.click();
    }

    /**
     * Checks the number of products in the wishlist on the Wishlist page
     * @param number number we need to compare with count of products
     *               in the wishlist
     * @return true if count of products matches with the given number
     */
    public boolean CheckProductsCountInWishlist(int number) {
        String wishlistProductsXpath = "//span[contains(text(), '"
                + number + "') and contains(@class, 'history__menu__num')]";
        return waitForElement(wishlistProductsXpath).isDisplayed();
    }

    /**
     * Clicks on the first product on page
     */
    public void OpenFirstProduct() {
        WebElement productCard = waitForElement(firstCardTitleXpath);
        productCard.click();
    }

    /**
     * One by one removes products from the wishlist
     * @param count count of products to remove
     */
    public void ClickRemoveOneBtn(int count) {
        for (int i=0; i<count; i++) {
            WebElement removeBtn =
                    waitForElement(removeProductFromWishBtnXpath);
            removeBtn.click();
        }
    }

    /**
     * Removes all products from the wishlist page
     */
    public void ClickRemoveAllBtn() {
        WebElement removeBtn = waitForElement(removeAllFromWishBtnXpath);
        removeBtn.click();
    }

    /**
     * Clicks on the «add to cart» button
     */
    public void AddToCart() {
        WebElement addToCartBtn = driver.findElementByXPath(addToCartBtnXpath);
        addToCartBtn.click();
    }

    /**
     * Clicks on the «add to cart selected products» button
     */
    public void AddToCartChecked() {
        WebElement addToCartChecked =
                driver.findElement(By.xpath(addToCartCheckedXpath));
        addToCartChecked.click();
    }

    /**
     * @return true if the wishlist is empty
     */
    public boolean IsEmptyWishlist() {
        return waitForElement(emptyWishMessageXpath).isDisplayed();
    }

}
