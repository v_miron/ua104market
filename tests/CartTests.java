package tests;

import org.junit.Assert;
import org.junit.Test;


public class CartTests extends pageObjects.BasePage {

    /**
     * Add products to cart from the front page, from the category page,
     * and from the product page. Check correct amount of products in cart
     */
    @Test
    public void TestAddToCart() {
        AddToCart(0);
        GoToGasCategory();
        AddToCart(2);
        OpenProductNum(3);
        AddToCartProductPage();
        Assert.assertTrue(CheckProductCountInCart(1, 2));
    }

    /**
     * Add to cart 2 products (from front page and category page),
     * open cart popup and remove 2 products. Check empty cart message
     */
    @Test
    public void TestRemoveFromCart() {
        AddToCart(0);
        GoToGasCategory();
        AddToCart(2);
        RemoveFromCart(0);
        RemoveFromCart(0);
        Assert.assertTrue(IsEmptyCart());
    }

    /**
     * Change count of items in the cart, check the correctness
     * of the total amount
     */
    @Test
    public void TestChangeQuantity() {
        AddToCart(0);
        int price = GetSumInCart();
        ChangeAmountCart(+1);
        boolean isPriseCorrect2 = GetSumInCart() == price*2;
        ChangeAmountCart(99);
        ChangeAmountCart(+1);
        boolean isPriseCorrect999 = GetSumInCart() == price*300;
        ChangeAmountCart(-1);
        boolean isPriseCorrect998 = GetSumInCart() == price*299;

        boolean isAllCorrect = isPriseCorrect2 &&
                isPriseCorrect999 && isPriseCorrect998;

        Assert.assertTrue(isAllCorrect);
    }

    /**
     * Continue shopping after add product to the cart
     */
    @Test
    public void TestContinueShopping() {
        GoToBoilerCategory();
        String url1 = GetCurrentUrl();
        AddToCart(0);
        ContinueShoppingCart();
        Assert.assertEquals(url1, GetCurrentUrl());
    }

    /**
     * Continue shopping after add product to the cart and change amount
     */
    @Test
    public void TestContinueShoppingAfterChangeAmount()
            throws InterruptedException {
        GoToBoilerCategory();
        AddToCart(0);
        Thread.sleep(4000);
        ChangeAmountCart(5);
        ContinueShoppingCart();
        OpenCart();
        CheckProductCountInCart(5, 0);
    }

    /**
     * Go to checkout page from cart popup
     */
    @Test
    public void TestGoToCheckoutFromCart() {
        GoToBoilerCategory();
        AddToCart(0);
        GoToCheckoutFromCart();
        String cartUrl = base_page + "ua/cart";
        Assert.assertEquals(cartUrl, GetCurrentUrl());
    }

    /**
     * Inability to go to the checkout page with empty cart
     */
    @Test
    public void TestCheckoutWithEmptyCart() {
        AddToCart(0);
        RemoveFromCart(0);
        boolean isEmpty = IsEmptyCart();
        String url = "ua/cart";
        OpenUrl(url);
        Assert.assertTrue(IsEmptyCart() && isEmpty);
    }

}
