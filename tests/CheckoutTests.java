package tests;

import com.github.javafaker.Faker;
import org.junit.Assert;
import org.junit.Test;
import pageObjects.CheckoutPage;

import java.util.Locale;
import java.util.logging.Logger;


public class CheckoutTests extends CheckoutPage {

    private static Logger log = Logger.
            getLogger(CheckoutTests.class.getName());

    Faker fake = new Faker(new Locale("ru"));
    String lastName = fake.name().lastName();
    String firstName = fake.name().firstName();
    String middleName = fake.name().firstName();
    String phone = fake.phoneNumber().cellPhone();
    String recipientLastName = fake.name().lastName();
    String recipientFirstName = fake.name().firstName();
    String recipientMiddleName = fake.name().firstName();
    String recipientPhone = fake.phoneNumber().cellPhone();
    String sameRecipient = "same";
    String otherRecipient = "other";
    String xssMessage = "<script>alert('Test');</script>";


    /**
     * Send all valid fields, go to payment page
     */
    @Test
    public void TestAllValidFields()
            throws InterruptedException {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SetDeliveryRegion(1);
        SetDeliveryCity(2);
        SetDeliveryWarehouse(0);
        Thread.sleep(5000);
        SelectRecipient(sameRecipient);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsPaymentPage());
    }

    /**
     * Send all valid fields, not accept personal data processing
     */
    @Test
    public void TestAllValidFieldsNotAcceptPD() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(sameRecipient);
        PersonalDataDoNotAccept();
        Submit();
        Assert.assertTrue(IsPersonalDataErrorMessage());
    }

    /**
     * Send invalid values to name field
     */
    @Test
    public void TestInvalidName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendName("a");
        boolean errorMsg = IsErrorMessage();
        SendName("clear");
        SendName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendName("clear");
        SendName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to last name field
     */
    @Test
    public void TestInvalidLastName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName("a");
        boolean errorMsg = IsErrorMessage();
        SendLastName("clear");
        SendLastName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendLastName("clear");
        SendLastName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendLastName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to middle name field
     */
    @Test
    public void TestInvalidMiddleName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendMiddleName("a");
        boolean errorMsg = IsErrorMessage();
        SendMiddleName("clear");
        SendMiddleName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendMiddleName("clear");
        SendMiddleName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendMiddleName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to phone field
     */
    @Test
    public void TestInvalidPhone() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendPhone("akz");
        SendPhone("!<>");
        SendPhone("вап");
        boolean errorMsg = IsErrorMessage();

        Assert.assertTrue(errorMsg);
    }

    /**
     * Send invalid values to recipient name field
     */
    @Test
    public void TestInvalidRecipientName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SelectRecipient("other");
        SendRecipientName("a");
        boolean errorMsg = IsErrorMessage();
        SendRecipientName("clear");
        SendRecipientName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendRecipientName("clear");
        SendRecipientName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendRecipientName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to recipient last name field
     */
    @Test
    public void TestInvalidRecipientLastName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SelectRecipient("other");
        SendRecipientLastName("a");
        boolean errorMsg = IsErrorMessage();
        SendRecipientLastName("clear");
        SendRecipientLastName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendRecipientLastName("clear");
        SendRecipientLastName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendRecipientLastName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to recipient middle name field
     */
    @Test
    public void TestInvalidRecipientMiddleName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SelectRecipient("other");
        SendRecipientMiddleName("a");
        boolean errorMsg = IsErrorMessage();
        SendRecipientMiddleName("clear");
        SendRecipientMiddleName("sl");
        boolean errorMsg2 = IsErrorMessage();
        SendRecipientMiddleName("clear");
        SendRecipientMiddleName("<1!");
        boolean errorMsg3 = IsErrorMessage();
        SendRecipientMiddleName("clear");

        Assert.assertTrue(errorMsg && errorMsg2 && errorMsg3);
    }

    /**
     * Send invalid values to recipient phone field
     */
    @Test
    public void TestInvalidRecipientPhone() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SelectRecipient("other");
        SendRecipientPhone("akz");
        SendRecipientPhone("!<>");
        SendRecipientPhone("вап");
        boolean errorMsg = IsErrorMessage();

        Assert.assertTrue(errorMsg);
    }

    /**
     * Send all valid fields with other recipient fields,
     * not accept personal data processing
     */
    @Test
    public void TestAllValidFieldsOtherRecipientNotAcceptPD() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(otherRecipient);
        SendRecipientLastName(recipientLastName);
        SendRecipientName(recipientFirstName);
        SendRecipientMiddleName(recipientMiddleName);
        SendRecipientPhone(recipientPhone);
        PersonalDataDoNotAccept();
        Submit();
        Assert.assertTrue(IsPersonalDataErrorMessage());
    }

    /**
     * Send all valid fields with other recipient fields,
     * go to payment page
     */
    @Test
    public void TestAllValidFieldsOtherRecipient()
            throws InterruptedException {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SetDeliveryRegion(1);
        SetDeliveryCity(2);
        SetDeliveryWarehouse(0);
        Thread.sleep(5000);
        SelectRecipient(otherRecipient);
        SendRecipientLastName(recipientLastName);
        SendRecipientName(recipientFirstName);
        SendRecipientMiddleName(recipientMiddleName);
        SendRecipientPhone(recipientPhone);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsPaymentPage());
    }

    /**
     * Send script to all fields, try to submit
     */
    @Test
    public void TestXSSVulnerability() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(xssMessage);
        boolean lastNameError = IsErrorLastName();
        SendName(xssMessage);
        boolean firstNameError = IsErrorFirstName();
        SendMiddleName(xssMessage);
        boolean middleNameError = IsErrorMiddleName();
        SendPhone(xssMessage);
        boolean phoneError = IsErrorPhone();
        SelectRecipient(otherRecipient);
        SendRecipientLastName(xssMessage);
        boolean recipientLastNameError = IsErrorRecipientLastName();
        SendRecipientName(xssMessage);
        boolean recipientFirstNameError = IsErrorRecipientFirstName();
        SendRecipientMiddleName(xssMessage);
        boolean recipientMiddleNameError = IsErrorRecipientMiddleName();
        SendRecipientPhone(xssMessage);
        boolean recipientPhoneError = IsErrorRecipientPhone();
        PersonalDataAccept();
        Submit();

        boolean isAllErrorMessages = lastNameError && firstNameError &&
                middleNameError && phoneError && recipientLastNameError &&
                recipientFirstNameError && recipientMiddleNameError &&
                recipientPhoneError;

        Assert.assertTrue(isAllErrorMessages);
    }

    /**
     * Send all valid fields except last name
     */
    @Test
    public void TestAllValidFieldsExceptLastName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(sameRecipient);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorLastName());
    }

    /**
     * Send all valid fields except first name
     */
    @Test
    public void TestAllValidFieldsExceptFirstName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(sameRecipient);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorFirstName());
    }

    /**
     * Send all valid fields except middle name
     */
    @Test
    public void TestAllValidFieldsExceptMiddleName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendPhone(phone);
        SelectRecipient(sameRecipient);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorMiddleName());
    }

    /**
     * Send all valid fields except phone
     */
    @Test
    public void TestAllValidFieldsExceptPhone() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SelectRecipient(sameRecipient);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorPhone());
    }

    /**
     * Send all valid fields except recipient last name
     */
    @Test
    public void TestAllValidFieldsExceptRecipientLastName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(otherRecipient);
        SendRecipientName(recipientFirstName);
        SendRecipientMiddleName(recipientMiddleName);
        SendRecipientPhone(recipientPhone);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorRecipientLastName());
    }

    /**
     * Send all valid fields except recipient first name
     */
    @Test
    public void TestAllValidFieldsExceptRecipientFirstName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(otherRecipient);
        SendRecipientLastName(recipientLastName);
        SendRecipientMiddleName(recipientMiddleName);
        SendRecipientPhone(recipientPhone);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorRecipientFirstName());
    }

    /**
     * Send all valid fields except recipient middle name
     */
    @Test
    public void TestAllValidFieldsExceptRecipientMiddleName() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(otherRecipient);
        SendRecipientLastName(recipientLastName);
        SendRecipientName(recipientFirstName);
        SendRecipientPhone(phone);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorRecipientMiddleName());
    }

    /**
     * Send all valid fields except recipient phone
     */
    @Test
    public void TestAllValidFieldsExceptRecipientPhone() {
        GoToGasCategory();
        AddToCart(1);
        GoToCheckoutFromCart();
        GoToCheckout();
        SendLastName(lastName);
        SendName(firstName);
        SendMiddleName(middleName);
        SendPhone(phone);
        SelectRecipient(otherRecipient);
        SendRecipientLastName(recipientLastName);
        SendRecipientName(recipientFirstName);
        SendRecipientMiddleName(recipientMiddleName);
        PersonalDataAccept();
        Submit();
        Assert.assertTrue(IsErrorRecipientPhone());
    }

}
