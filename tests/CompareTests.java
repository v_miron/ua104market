package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;


public class CompareTests extends pageObjects.ComparePage {

    /**
     * Check presents of compare buttons on the homepage
     */
    @Test
    public void TestComparePresentHome() {
        Assert.assertEquals(CountProducts(), CountCompareBlocks());
    }

    /**
     * Check presents of compare buttons on the category page
     */
    @Test
    public void TestComparePresentCategory() {
        GoToGasCategory();
        Assert.assertEquals(CountProducts(), CountCompareBlocks());
    }

    /**
     * Check presents of compare buttons on the product page
     */
    @Test
    public void TestComparePresentProductPage() {
        GoToGasCategory();
        OpenProductNum(0);
        Assert.assertTrue(IsCompareBtnProductPage());
    }

    /**
     * Add to compare list 5 products and check them on the compare page
     */
    @Test
    public void TestAddToCompare() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(2);

        GoToBoilerCategory();
        AddToCompareCardNum(0);

        GoToCountersCategory();
        OpenProductNum(0);
        AddToCompFromTopProdPage();

        GoToOthersCategory();
        OpenProductNum(0);
        AddToCompFromSideProdPage();

        GoToCompare();

        Assert.assertTrue(CountComparedItems(5));
    }

    /**
     * Add to compare list 5 products and remove
     * them on the compare page one by one
     */
    @Test
    public void TestRemoveFromCompare() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(2);

        GoToBoilerCategory();
        AddToCompareCardNum(0);

        GoToCountersCategory();
        OpenProductNum(0);
        AddToCompFromTopProdPage();

        GoToOthersCategory();
        OpenProductNum(0);
        AddToCompFromSideProdPage();

        GoToCompare();

        RemoveCardNum(0);
        RemoveCardNum(3);
        RemoveCardNum(2);
        RemoveCardNum(1);

        Assert.assertTrue(isEmptyCompare());
    }

    /**
     * Adds to compare list 5 products and removes
     * them on the compare page at once
     */
    @Test
    public void TestRemoveAllFromCompare() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(2);

        GoToBoilerCategory();
        AddToCompareCardNum(0);

        GoToCountersCategory();
        OpenProductNum(0);
        AddToCompFromTopProdPage();

        GoToOthersCategory();
        OpenProductNum(0);
        AddToCompFromSideProdPage();

        GoToCompare();

        RemoveAll();

        Assert.assertTrue(isEmptyCompare());
    }

    /**
     * Check work of filter all characteristic / only differences
     */
    @Test
    public void TestCompareListFilter() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(1);
        AddToCompareCardNum(2);

        GoToCompare();

        int countAll = CountCharacteristicRows(3);
        SelectOnlyDiffsFilter();
        int countDiffs = CountCharacteristicRows(3);

        Assert.assertTrue(countAll <= countDiffs);
    }

    /**
     * Checks navigation arrows on the compare page
     */
    @Test
    public void TestNavigateCompareItems() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(1);
        AddToCompareCardNum(2);
        AddToCompareCardNum(3);
        AddToCompareCardNum(4);

        GoToCompare();

        NavigateNextItem();
        NavigatePrevItem();
    }

    /**
     * Adds products to cart from compare list
     */
    @Test
    public void TestAddToCartFromCompare() {
        GoToGasCategory();
        AddToCompareCardNum(0);
        AddToCompareCardNum(2);

        GoToCompare();
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("window.scrollBy(0, 2000);");

        AddToCart(2);
        ContinueShoppingCart();

        Assert.assertTrue(MenuCartCounter(1));
    }
}
