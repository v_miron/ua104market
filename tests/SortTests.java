package tests;

import org.junit.Assert;
import org.junit.Test;
import pageObjects.SortFilterPage;

import java.util.logging.Logger;


public class SortTests extends SortFilterPage {

    private static Logger log = Logger.
            getLogger(SortTests.class.getName());

    /**
     * Select sorting by rating, check its correct
     */
    @Test
    public void TestSortByRating() throws InterruptedException {
        GoToGasCategory();
        int numOfProducts = CountProducts();
        SortByRating();
        Thread.sleep(7000);
        double tempRate = 5;
        boolean isSortCorrect = false;


        for (int i = 0; i < numOfProducts; i++) {
            double currentRate = GetCardRate(i);
            if (currentRate <= tempRate) {
                isSortCorrect = true;
                tempRate = currentRate;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " product has a higher rating than "
                        + i + " product");
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select sorting by ascending price, check its correct
     */
    @Test
    public void TestSortByPriceAsc() throws InterruptedException {
        GoToGasCategory();
        int numOfProducts = CountProducts();
        SortPriceAsc();
        Thread.sleep(7000);
        int tempPrice = 0;
        boolean isSortCorrect = false;

        for (int i = 0; i < numOfProducts; i++) {
            int currentPrice = GetCardPrice(i);
            if (currentPrice >= tempPrice) {
                isSortCorrect = true;
                tempPrice = currentPrice;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " product has lower price than "
                        + i + "product");
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select sorting by descending price, check its correct
     */
    @Test
    public void TestSortByPriceDesc() throws InterruptedException {
        GoToGasCategory();
        SortPriceDesc();
        Thread.sleep(6000);
        int numOfProducts = CountProducts();
        int tempPrice = 1000000;
        boolean isSortCorrect = false;

        for (int i = 0; i < numOfProducts; i++) {
            int currentPrice = GetCardPrice(i);
            if (currentPrice <= tempPrice) {
                isSortCorrect = true;
                tempPrice = currentPrice;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " product has higher price than "
                        + i + "product");
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select sorting new first, check its correct
     */
    @Test
    public void TestNewFirst() {
        GoToGasCategory();
        SortNewFirst();
        int productsNum = CountProducts();
        int newCounter = 0;
        boolean correctSort = false;

        for (int i=0; i < productsNum; i++) {
            if (CheckNewLabel(i)) {
                newCounter += 1;
            }
        }
        if (newCounter != 0) {
            for (int i=newCounter; i < productsNum; i++){
                correctSort = !CheckNewLabel(i);
            }
        }

        if (newCounter != 0) {
            Assert.assertTrue(correctSort);
        } else {
            Assert.assertTrue(true);
            log.info("No items with 'new' label");
        }

    }

    /**
     * Set price range, check if result correct
     */
    @Test
    public void TestPriceRange() throws InterruptedException {
        String min = "25000";
        String max = "40000";
        int minInt = Integer.parseInt(min);
        int maxInt = Integer.parseInt(max);

        GoToGasCategory();
        SetPriceRange(min, max);
        boolean isSortCorrect = false;

        int numOfProducts = CountProducts();

        for (int i = 0; i < numOfProducts; i++) {
            int currentPrice = GetCardPrice(i);
            isSortCorrect = currentPrice > minInt && currentPrice < maxInt;
            if (!isSortCorrect) {
                log.info((i+1) + " product have price: " + currentPrice
                        + " .It's not in range: " + min + " and " + max);
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Set invalid price range, check if result correct
     */
    @Test
    public void TestInvalidPriceRange() throws InterruptedException {
        String min = "40000";
        String max = "25000";
        int minInt = Integer.parseInt(min);
        int maxInt = Integer.parseInt(max);

        GoToGasCategory();
        SetPriceRange(min, max);
        Thread.sleep(5000);
        boolean isSortCorrect = false;

        int numOfProducts = CountProducts();

        for (int i = 0; i < numOfProducts; i++) {
            int currentPrice = GetCardPrice(i);
            isSortCorrect = currentPrice > maxInt && currentPrice < minInt;
            if (!isSortCorrect) {
                log.info((i+1) + " product have price: " + currentPrice
                        + " .It's not in range: " + max + " and " + min);
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select three items in the sidebar filter. Opens the products from
     * filtering results and checks for the presence of the previously
     * selected parameters
     */
    @Test
    public void TestFilterByCharacteristics()
            throws InterruptedException {
        GoToGasCategory();
        SelectFilters();
        boolean isCorrect = false;
        int productsCount = CountProducts();

        for (int i=0; i < productsCount; i++) {
            OpenProductNum(i);
            OpenCharacteristicTab();
            isCorrect = IsCorrectCharacteristic();
            driver.navigate().back();
        }
        Assert.assertTrue(isCorrect);
    }

}
