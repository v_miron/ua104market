package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/** Runs all tests */
@RunWith(Suite.class)
@Suite.SuiteClasses({CallBackTests.class, CartTests.class, CheckoutTests.class,
        tests.CompareTests.class, ReviewTests.class, SearchTests.class,
        SortTests.class, tests.WishlistTests.class, DeliveryTests.class} )
public class SuiteTestClass {
}
