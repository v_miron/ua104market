package tests;

import com.github.javafaker.Faker;
import org.junit.Assert;
import org.junit.Test;
import pageObjects.CallbackPage;

import java.util.Locale;


public class CallBackTests extends CallbackPage {

    Faker fake = new Faker(new Locale("ru"));
    String name = fake.name().firstName() + " " + fake.name().lastName();
    String phone = fake.phoneNumber().cellPhone();
    String xssMessage = "<script>alert('Test');</script>";

    /**
     * Open callback popup, fill all fields, submits
     * Check presents of success message
     */
    @Test
    public void TestValidNamePhone() {
        OpenCallback();
        SendName(name);
        SendPhone(phone);
        SubmitCallback();
        Assert.assertTrue(IsSuccessMessage());
    }

    /**
     * Open callback popup, fill name field, push submit,
     * check if there phone error message
     */
    @Test
    public void TestValidNameInvalidPhone() {
        OpenCallback();
        SendName(name);
        SubmitCallback();
        Assert.assertTrue(IsErrorPhone());
    }

    /**
     * Open callback popup, fill phone field, push submit,
     * check if there name error message
     */
    @Test
    public void TestInvalidNameValidPhone() {
        OpenCallback();
        SendPhone(phone);
        SubmitCallback();
        Assert.assertTrue(IsErrorName());
    }

    /**
     * Open callback popup, push submit, check if there
     * name and phone error messages
     */
    @Test
    public void TestInvalidAllFields() {
        OpenCallback();
        SubmitCallback();
        Assert.assertTrue(IsErrorName() && IsErrorPhone());
    }

    /**
     * Open callback popup, send xssMessage to all fields, push submit,
     * check if there name and phone error messages
     */
    @Test
    public void TestXSSVulnerability() {
        OpenCallback();
        SendName(xssMessage);
        SendPhone(xssMessage);
        SubmitCallback();
        Assert.assertTrue(IsErrorName() && IsErrorPhone());
    }

    /**
     * Open callback popup, send invalid data to name field,
     * check if there name error message
     */
    @Test
    public void TestInvalidName() {
        OpenCallback();
        SendName("a");
        SendName("ss");
        SendName("<1!");
        Assert.assertTrue(IsErrorName());
    }

    /**
     * Open callback popup, send invalid data to phone field,
     * check if there name error message
     */
    @Test
    public void TestInvalidPhone() {
        OpenCallback();
        SendPhone("akz");
        SendPhone("!<>");
        SendPhone("вап");
        Assert.assertTrue(IsErrorPhone());
    }

}
