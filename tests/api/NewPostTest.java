package tests.api;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Test;

import java.util.List;

public class NewPostTest extends NewPostMethods{

    @Test
    public void testDeliveryCost() {
        String requestBody = """
                {
                   "modelName": "InternetDocument",
                   "calledMethod": "getDocumentPrice",
                   "methodProperties": {
                      "CitySender": "e718a680-4b33-11e4-ab6d-005056801329",
                      "CityRecipient": "fd249301-887a-11e9-898c-005056b24375",
                      "Weight": "2",
                      "ServiceType": "WarehouseWarehouse",
                      "Cost": "14000",
                      "CargoType": "Cargo",
                      "SeatsAmount": "1",
                        "PackCalculate": {
                            "PackCount": "1",
                            "PackRef": "1499fa4a-d26e-11e1-95e4-0026b97ed48a"
                        },
                      "RedeliveryCalculate": {
                         "CargoType": "Money",
                         "Amount": "14000"
                      },
                      "OptionsSeat": [
                      {
                        "weight": 2,
                        "volumetricWidth":20,
                        "volumetricLength": 10,
                        "volumetricHeight": 5
                      }
                    ]
                   },
                   "apiKey": "18d6cb10f6466af70d79c7c62b05a389"
                }
                """;
        Response response = doGetRequest(requestBody);
        String pathProductPrice = "data.AssessedCost";
        String pathDeliveryCost = "data.Cost";
        String pathReDeliveryCost = "data.CostRedelivery";
        String pathPackPrice = "data.CostPack";

        String ProductPrice = JsonPath.from(
                response.asString()).getString(pathProductPrice);
        String DeliveryCost = JsonPath.from(
                response.asString()).getString(pathDeliveryCost);
        String ReDeliveryCost = JsonPath.from(
                response.asString()).getString(pathReDeliveryCost);
        String PackPrice = JsonPath.from(
                response.asString()).getString(pathPackPrice);

        System.out.println("Result of calculating delivery cost:");
        System.out.println("Product price: " + ProductPrice);
        System.out.println("Delivery cost: " + DeliveryCost);
        System.out.println("ReDelivery cost: " + ReDeliveryCost);
        System.out.println("Pack price: " + PackPrice);
    }

    @Test
    public void testCitySearch() {
        String city = "Бучач";
        int resultLimit = 0;
        String requestBody = String.format("""
                {
                "apiKey": "18d6cb10f6466af70d79c7c62b05a389",
                 "modelName": "Address",
                    "calledMethod": "searchSettlements",
                    "methodProperties": {
                        "CityName": "%s",
                        "Limit": %s
                    }
                }
                """, city, resultLimit);
        Response response = doGetRequest(requestBody);
        String pathCity = "data.Addresses.Present";
        String pathWarehouse = "data.Addresses.Warehouses";

        List<String> cityList = convertCitiesStringToList
                (JsonPath.from(response.asString()).getString(pathCity));
        List<String> warehouseList = convertWarehouseStringToList
                (JsonPath.from(response.asString()).getString(pathWarehouse));

        System.out.println("Results of search by city \"" + city + "\" is:");

        for(int i = 0; i < cityList.size(); i++) {
            System.out.println(cityList.get(i) +
                    " (кількість складів: " + warehouseList.get(i) + ")");
        }
    }

}
