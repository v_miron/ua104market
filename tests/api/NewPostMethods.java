package tests.api;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class NewPostMethods {

    private static RequestSpecification spec;

    @Before
    public void initSpec() {
        spec = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri("https://api.novaposhta.ua/v2.0/json/")
//                .addFilter(new ResponseLoggingFilter())
//                .addFilter(new RequestLoggingFilter())
                .build();
    }

    public static Response doGetRequest(String requestBody) {
        RestAssured.defaultParser = Parser.JSON;

        return given()
                .spec(spec)
                .when().body(requestBody).post()
                .then().contentType(ContentType.JSON)
                .extract().response();
    }

    public List<String> convertCitiesStringToList(String response) {
        response = response.replace("[[", "");
        response = response.replace("]]", "");
        return Arrays.asList(response.split("\\., "));
    }

    public List<String> convertWarehouseStringToList(String response) {
        response = response.replace("[[", "");
        response = response.replace("]]", "");
        return Arrays.asList(response.split(", "));
    }

}
