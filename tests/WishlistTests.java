package tests;

import org.junit.Assert;
import org.junit.Test;
import pageObjects.WishlistPage;


public class WishlistTests extends WishlistPage {

    /**
     * Adds 4 products to the wishlist, checks it on the Wishlist page
     */
    @Test
    public void TestAddToWishlistFromCategory() {
        GoToGasCategory();
        AddToWishCardNum(0);

        GoToBoilerCategory();
        AddToWishCardNum(0);

        GoToCountersCategory();
        AddToWishCardNum(0);

        GoToOthersCategory();
        AddToWishCardNum(0);

        GoToWishlist();

        Assert.assertTrue(CheckProductsCountInWishlist(4));
    }

    /**
     * Adds 3 products to the wishlist from the product pages,
     * checks it on the Wishlist page
     */
    @Test
    public void TestAddToWishlistFromProduct() {
        GoToGasCategory();
        OpenFirstProduct();
        AddToWishProductPage();

        GoToBoilerCategory();
        OpenFirstProduct();
        AddToWishProductPage();

        GoToCountersCategory();
        OpenFirstProduct();
        AddToWishProductPageRightBtn();

        GoToWishlist();

        Assert.assertTrue(CheckProductsCountInWishlist(3));
    }

    /**
     * Remove from the wishlist products one by one
     */
    @Test
    public void TestRemoveFromWishList() {
        GoToGasCategory();
        AddToWishCardNum(0);

        GoToBoilerCategory();
        AddToWishCardNum(0);

        GoToCountersCategory();
        AddToWishCardNum(0);

        GoToWishlist();

        ClickRemoveOneBtn(3);

        Assert.assertTrue(IsEmptyWishlist());
    }

    /**
     * Remove from the wishlist all products
     */
    @Test
    public void TestRemoveAllFromWishList() {
        GoToGasCategory();
        AddToWishCardNum(0);

        GoToBoilerCategory();
        AddToWishCardNum(0);

        GoToCountersCategory();
        AddToWishCardNum(0);

        GoToWishlist();

        ClickRemoveAllBtn();

        Assert.assertTrue(IsEmptyWishlist());
    }

    /**
     * Add product to the cart from wishlist
     */
    @Test
    public void TestAddToCartFromWish() {
        GoToGasCategory();
        AddToWishCardNum(0);

        GoToBoilerCategory();
        AddToWishCardNum(0);

        GoToCountersCategory();
        AddToWishCardNum(0);

        GoToWishlist();

        AddToCart();

        Assert.assertTrue(CheckProductCountInCart(1, 0));
    }

    /**
     * Add to wishlist 9 products from 3 categories, than go to the
     * wishlist page, select 4 checkboxes on products and add them to card
     */
    @Test
    public void TestAddSeveralToCartFromWish() throws InterruptedException {
        GoToGasCategory();
        AddToWishCardNum(0);
        Thread.sleep(1000);
        AddToWishCardNum(2);
        Thread.sleep(1000);
        AddToWishCardNum(4);
        Thread.sleep(1000);

        GoToBoilerCategory();
        AddToWishCardNum(0);
        Thread.sleep(1000);
        AddToWishCardNum(2);
        Thread.sleep(1000);
        AddToWishCardNum(4);
        Thread.sleep(1000);

        GoToCountersCategory();
        AddToWishCardNum(0);
        Thread.sleep(1000);
        AddToWishCardNum(2);
        Thread.sleep(1000);
        AddToWishCardNum(4);
        Thread.sleep(1000);

        GoToWishlist();

        CheckProductCheckboxNum(2);
        CheckProductCheckboxNum(3);
        CheckProductCheckboxNum(4);
        CheckProductCheckboxNum(5);

        AddToCartChecked();
        Thread.sleep(10000);
        Assert.assertTrue(CheckProductCountInCart(1, 3));
    }

    /**
     * Add to wishlist 6 products from 2 categories, than go to the
     * wishlist page, select all products and add them to card
     */
    @Test
    public void AddAllToCartFromWish() throws InterruptedException {
        GoToGasCategory();
        AddToWishCardNum(0);
        Thread.sleep(2000);
        AddToWishCardNum(2);
        Thread.sleep(2000);
        AddToWishCardNum(4);
        Thread.sleep(2000);

        GoToBoilerCategory();
        AddToWishCardNum(0);
        Thread.sleep(2000);
        AddToWishCardNum(2);
        Thread.sleep(2000);
        AddToWishCardNum(4);
        Thread.sleep(2000);

        GoToWishlist();

        CheckAllCheckbox();
        AddToCartChecked();
        Thread.sleep(10000);

        Assert.assertTrue(CheckProductCountInCart(1, 5));
    }

}
