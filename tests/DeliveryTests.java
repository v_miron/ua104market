package tests;

import org.junit.Assert;
import org.junit.Test;
import pageObjects.CheckoutPage;


public class DeliveryTests extends CheckoutPage {

    String noDeliveryPriceMsg = "Уточнюйте у менеджера";

    /**
     * Check that delivery cost is > 0 in case product weight is not 0
     */
    @Test
    public void TestDeliveryWithWeight() throws InterruptedException {
        GoToCountersCategory();
        AddToCart(0);
        GoToCheckoutFromCart();
        GoToCheckout();
        Thread.sleep(5000);
        Assert.assertTrue(Integer.parseInt(GetDeliveryPrice()) > 0);
    }

    /**
     * Check that there is no delivery cost displayed in case
     * product does not have weight
     */
    @Test
    public void TestDeliveryWithoutWeight() throws InterruptedException {
        GoToOthersCategory();
        AddToCart(0);
        GoToCheckoutFromCart();
        GoToCheckout();
        Thread.sleep(5000);
        Assert.assertEquals(GetDeliveryPrice(), noDeliveryPriceMsg);
    }

    /**
     * Check that there is no delivery cost displayed in case
     * that there is two products in cart: one product does not have weight,
     * second product have weight
     */
    @Test
    public void TestDeliveryWithAndWithoutWeight()
            throws InterruptedException {
        GoToCountersCategory();
        AddToCart(0);
        GoToOthersCategory();
        AddToCart(0);
        GoToCheckoutFromCart();
        GoToCheckout();
        Thread.sleep(5000);
        Assert.assertEquals(GetDeliveryPrice(), noDeliveryPriceMsg);
    }

    /**
     * Check that there is no delivery cost displayed in case
     * that there is three products in cart: one product does not have weight,
     * second and third products have weight
     */
    @Test
    public void TestDeliverySeveralWithAndWithoutWeight()
            throws InterruptedException {
        GoToCountersCategory();
        AddToCart(0);
        GoToGasCategory();
        AddToCart(0);
        GoToOthersCategory();
        AddToCart(0);
        GoToCheckoutFromCart();
        GoToCheckout();
        Thread.sleep(5000);
        Assert.assertEquals(GetDeliveryPrice(), noDeliveryPriceMsg);
    }

}
