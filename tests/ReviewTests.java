package tests;

import org.junit.Assert;
import org.junit.Test;
import pageObjects.ReviewPage;

import java.text.ParseException;
import java.util.Date;
import java.util.logging.Logger;


public class ReviewTests extends ReviewPage {

    private static Logger log = Logger.getLogger(ReviewTests.class.getName());

    String benefit = "Крутой товар!";
    String disadvantage = "Но не во всем";
    String comment = "Купили товар недавно, особых " +
            "восторгов не вызывает, но работает";
    String complaintComment = "Задається, це брехливий коментар";
    String name = "Валентиныч";
    String email = "valik@valik.com";
    String xssMessage = "<script>alert('Test');</script>";

    /**
     * Check presents of rating blocks on homepage
     */
    @Test
    public void TestRatingPresentHome() {
        Assert.assertEquals(CountProducts(), CountRatingBlocks());
    }

    /**
     * Check presents of rating blocks on category page
     */
    @Test
    public void TestRatingPresentCategory() {
        GoToGasCategory();
        Assert.assertEquals(CountProducts(), CountRatingBlocks());
    }

    /**
     * Check presents of rating blocks on product page
     */
    @Test
    public void TestRatingPresentProductPage() {
        OpenProductNum(0);
        Assert.assertTrue(IsRatingOnProductPage());
    }

    /**
     * Send to review form all valid data
     */
    @Test
    public void TestReviewFormAllValid() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SetRatingReviewInPopup(5);
        SendBenefits(benefit);
        SendDisadvantages(disadvantage);
        SendComment(comment);
        SendName(name);
        SendEmail(email);
        CheckReceiveAnswer();
        SubmitReview();

        Assert.assertTrue(IsSuccessMsg());
    }

    /**
     * Send to review form only valid name and comment
     */
    @Test
    public void TestReviewFormOnlyNameComment() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SetRatingReviewInPopup(4);
        SendComment(comment);
        SendName(name);
        SubmitReview();

        Assert.assertTrue(IsSuccessMsg());
    }

    /**
     * Send to review form all valid data except comment (it's empty)
     */
    @Test
    public void TestReviewFormAllValidExceptComment() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SetRatingReviewInPopup(3);
        SendBenefits(benefit);
        SendDisadvantages(disadvantage);
        SendName(name);
        SendEmail(email);
        CheckReceiveAnswer();
        SubmitReview();

        Assert.assertTrue(IsCommentErrorMsg());
    }

    /**
     * Send to review form all valid data except name (it's empty)
     */
    @Test
    public void TestReviewFormAllValidExceptName() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SetRatingReviewInPopup(3);
        SendBenefits(benefit);
        SendDisadvantages(disadvantage);
        SendComment(comment);
        SendEmail(email);
        CheckReceiveAnswer();
        SubmitReview();

        Assert.assertTrue(IsNameErrorMsg());
    }

    /**
     * Send to review form all valid data except comment and name (it's empty)
     */
    @Test
    public void TestReviewFormAllValidExceptNameComment() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SetRatingReviewInPopup(3);
        SendBenefits(benefit);
        SendDisadvantages(disadvantage);
        SendEmail(email);
        CheckReceiveAnswer();
        SubmitReview();

        Assert.assertTrue(IsCommentErrorMsg() && IsNameErrorMsg());
    }

    /**
     * Submit empty review form
     */
    @Test
    public void TestReviewFormAllInvalid() {
        OpenProductNum(0);
        OpenReviewTab();
        OpenReviewForm();
        SubmitReview();

        Assert.assertTrue(IsCommentErrorMsg() && IsNameErrorMsg());
    }

//    @Test
//    public void TestReviewFormXSSVulnerability() {
//        OpenProductNum(0);
//        OpenReviewTab();
//        OpenReviewForm();
//        SetRatingReviewInPopup(5);
//        SendBenefits(xssMessage);
//        SendDisadvantages(xssMessage);
//        SendComment(xssMessage);
//        SendName(xssMessage);
////        SendEmail(xssMessage);
//        SubmitReview();
//        driver.get("https://backend-shop-srv-staging.ent.ukrgas.com.ua" +
//                "/admin/admin/index/index/key/b1f37351096fc3795fdb82" +
//                "c185466cfb924c9c24c3c3fa54324f22bbb4866a74/");
//        driver.findElement(By.xpath("//input[@id='username']")).sendKeys("admin4");
//        driver.findElement(By.xpath("//input[@id='login']")).sendKeys("admin41.magento.104");
//        driver.findElement(By.xpath("//button[@class='action-login action-primary']")).click();
//        driver.findElement(By.xpath("//li[@id='menu-magento-backend-marketing']")).click();
//        driver.findElement(By.xpath("//span[contains(text(), 'Pending')]")).click();
//        boolean isMessage = driver.findElement(By.xpath
//                ("//td[contains(text(), 'Test xss')]")).isDisplayed();
//        driver.quit();
//        Assert.assertTrue(isMessage);
//    }

    /**
     * Select sorting review by best, check if it correct
     */
    @Test
    public void TestSortReviewsByBest() throws InterruptedException {
        GoToOthersCategory();
        OpenProductNum(0);
        OpenReviewTab();
        SortByBest();
        Thread.sleep(4000);
        LoadMoreReviews();
        Thread.sleep(4000);
        int numOfReviews = CountReviewBlocks();
        double tempRate = 5;
        boolean isSortCorrect = false;

        for (int i = 1; i < numOfReviews; i++) {
            double currentRate = GetReviewRate(i);
            if (currentRate <= tempRate) {
                isSortCorrect = true;
                tempRate = currentRate;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " product has a higher rating than " + i + " product");
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select sorting review by worst, check if it correct
     */
    @Test
    public void TestSortReviewsByWorst() throws InterruptedException {
        GoToOthersCategory();
        OpenProductNum(0);
        OpenReviewTab();
        SortByWorst();
        Thread.sleep(4000);
        LoadMoreReviews();
        Thread.sleep(4000);
        int numOfReviews = CountReviewBlocks();
        double tempRate = 0;
        boolean isSortCorrect = false;

        for (int i = 1; i < numOfReviews; i++) {
            double currentRate = GetReviewRate(i);
            if (currentRate >= tempRate) {
                isSortCorrect = true;
                tempRate = currentRate;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " product has a lower rating than "
                        + i + " product");
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Select sorting review by date, check if it correct
     */
    @Test
    public void TestSortReviewsByNew()
            throws InterruptedException, ParseException {
        GoToOthersCategory();
        OpenProductNum(0);
        OpenReviewTab();
        SortByDate();
        Thread.sleep(4000);
        LoadMoreReviews();
        Thread.sleep(4000);
        int numOfReviews = CountReviewBlocks() - 1;
        Date tempDate = new Date();
        boolean isSortCorrect = false;

        for (int i = 0; i < numOfReviews; i++) {
            Date currentDate = GetReviewDate(i);
            if (currentDate.toInstant().isBefore(tempDate.toInstant())
                    || currentDate.compareTo(tempDate) == 0) {
                isSortCorrect = true;
                tempDate = currentDate;
            } else {
                isSortCorrect = false;
                log.info((i+1) + " is earlier than " + i);
                break;
            }
        }
        Assert.assertTrue(isSortCorrect);
    }

    /**
     * Submit complaint form with comment and all checked complaint reasons
     */
    @Test
    public void TestComplaintAllValid() {
        GoToOthersCategory();
        OpenProductNum(0);
        OpenReviewTab();
        OpenComplaintForm(0);
        SetComplaintReason(0);
        SendComplaintComment(complaintComment);
        SubmitComplaint();
        Assert.assertTrue(IsComplaintSuccess());
    }

    /**
     * Submit empty complaint form
     */
    @Test
    public void TestComplaintAllEmpty() {
        GoToOthersCategory();
        OpenProductNum(0);
        OpenReviewTab();
        OpenComplaintForm(0);
        SetComplaintReason(1);
        SubmitComplaint();
        Assert.assertTrue(IsComplaintSuccess());
    }

//    @Test
//    public void TestComplaintXSS() {
//        GoToOthersCategory();
//        OpenProductNum(0);
//        OpenReviewTab();
//        OpenComplaintForm(0);
//        SendComplaintComment(xssMessage);
//        SubmitComplaint();
//        Assert.assertTrue(IsComplaintSuccess());
//    }

    /**
     * Check correctness of average product rating
     */
    @Test
    public void TestProductRatingCorrectness() throws InterruptedException {
        for (int i = 0; i <= 3; i++) {
            GoToOthersCategory();
            OpenProductNum(i);
            OpenReviewTab();
            LoadMoreReviews();
            Thread.sleep(4000);
            int numOfReviews = CountReviewBlocks();
            double averageRating = 0;

            if (numOfReviews > 0) {
                for (int j = 1; j < numOfReviews; j++) {
                    averageRating += GetReviewRate(j);
                }
                double resultDouble = (averageRating/(numOfReviews-1));
                String result = String.format("%.1f",
                        resultDouble).replace(",", ".");
                String averageRate = GetAverageRating().
                        replace(",", ".");
                Assert.assertEquals(result, averageRate);
            } else {
                Assert.assertTrue(true);
            }
        }
    }
}
