package tests;

import org.junit.Assert;
import org.junit.Test;
import pageObjects.BasePage;

import java.util.logging.Logger;


public class SearchTests extends BasePage {

    public static Logger log = Logger.getLogger(SearchTests.class.getName());

    public String searchRequestPositive = "Котел ";
    public String searchRequestNegative = "Ноутбук";
    public String searchRequestXSS = "<script>alert('aa');</script>";

    /**
     * Sends a search request and check result
     */
    @Test
    public void TestSearchPositive() {
        sendSearchRequest(searchRequestPositive);
        boolean result = waitForElement(searchResultXpath).isDisplayed();

        if (result) {
            Assert.assertTrue(true);
        } else {
            Assert.fail();
            log.info("Search result is not displayed");
        }
    }


    /**
     * Sends an invalid search request and check no result message
     */
    @Test
    public void TestSearchNegative() {
        sendSearchRequest(searchRequestNegative);
        boolean result = waitForElement(noResultMessageXpath).isDisplayed();

        if (result) {
            Assert.assertTrue(true);
        } else {
            Assert.fail();
            log.info("No result message is not displayed");
        }
    }

    /**
     * Check xss vulnerability in search field
     */
    @Test
    public void TestSearchXSSCheck() {
        sendSearchRequest(searchRequestXSS);
        boolean result = waitForElement(noResultMessageXpath).isDisplayed();

        if (result) {
            Assert.assertTrue(true);
        } else {
            Assert.fail();
            log.info("No result message is not displayed");
        }
    }

}