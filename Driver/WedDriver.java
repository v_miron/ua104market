package Driver;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class WedDriver {

    ChromeOptions options = new ChromeOptions();
    public ChromeDriver driver;
//    public String base_page = "https://market.104.ua/"; // prod url
    String hideInfoPanelOnStaging = "//a[@class='hide-button']";
    public String base_page =
            "https://frontend-shop-srv-staging.ent.ukrgas.com.ua/"; // staging url
    String acceptCookiesXpath = "//button[@class='btn green']";

    /**
     * Run browser, opan home page, accept cookies
     */
    @Before
    public void setUp() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",
                "/dev/chromedriver_last_version__20-01-20201.exe");
        driver = new ChromeDriver(options.setHeadless(true)); // running tests without running chrome
//        driver = new ChromeDriver(); // running tests with running chrome
//        driver.manage().window().maximize();
        Dimension d = new Dimension(1920,1080);
        driver.manage().window().setSize(d); // set window size
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(base_page);
        driver.findElement(By.xpath(hideInfoPanelOnStaging)).click(); // hiding symphony panel
        Thread.sleep(2000); // waiting for cookies panel
        driver.findElement(By.xpath(acceptCookiesXpath)).click(); // accepting cookies
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
